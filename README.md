### Requirements:
* docker
* docker-compose

### Installation:
1. git clone https://gitlab.com/AlexKird/timeweb_test.git
2. cd timeweb_test
3. cp .env.example .env
4. nano .env
5. _change DOMAIN and EMAIL variables in .env to yours_
6. make build
7. make start


### How to use:
#### 1. Start parsing

*POST* https://example.com/parse

reuqest: `{"url": "ya.ru"}`

answer: `{"id": "some-guid"}`

#### 2. Get results

*GET* https://example.com/results/some-guid

Answers:

if job done: `{"result": "https://example.com/download/some-guid.zip"}`

if job in progress: `{"result": "started"}`
