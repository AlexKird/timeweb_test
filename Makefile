include .env
export

build:
	docker-compose build $(SERVICES)

start:
	docker-compose up -d $(SERVICES)
	docker-compose restart workers

stop:
	docker-compose down -v

restart:
	make stop
	make start

dc-logs:
	docker-compose logs -f

logs_parser:
	tail -f app/logs/parser.log

logs_flask:
	tail -f app/logs/flask.log

cmd:
	docker-compose exec bot /bin/bash

list:
	docker-compose ps

rebuilt:
	make stop
	make build --no-cache
	make start

pull:
	git pull origin master;

update:
	make pull
	make restart
