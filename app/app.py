from flask import Flask, request, send_file, url_for, jsonify
from utils import flask_logger as logger
from scrapper import Parser
from dotenv import load_dotenv, find_dotenv
from os import getenv
load_dotenv(find_dotenv())
DOMAIN = getenv('DOMAIN')
app = Flask(__name__)


def log(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f'error {e} while request {request.url}, data: {request.data}')
            logger.exception(e)
            return {'result': 'error', 'error': str(type(e))}, 500

    wrapper.__name__ = func.__name__
    return wrapper


@app.route('/parse', methods=('POST', ))
@log
def parse():
    url = request.get_json()['url']
    parser = Parser.create_job(url)
    return jsonify({'id': parser.id})


@app.route('/results/<_id>')
@log
def results(_id):
    result, is_done = Parser.get_results(_id)
    if is_done:
        return jsonify(
            {'result': f'https://{DOMAIN}/download/{result}.zip'})
    else:
        return jsonify({'result': result})


@app.route('/download/<filename>')
@log
def download(filename):
    return send_file(f'./results/{filename}',
                     attachment_filename=f'{filename}',
                     as_attachment=True)

