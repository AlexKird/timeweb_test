from uuid import uuid4
from rq import Queue
from rq.queue import Job
from redis import Redis
from utils import parse_logger as logger
import shutil
from settings import REDIS_HOST, REDIS_PORT
from bs4 import *
from urllib.parse import urljoin
import os
from os.path import basename
import requests
from datetime import datetime
import concurrent.futures


redis = Redis(host=REDIS_HOST, port=REDIS_PORT)
queue = Queue(connection=redis, name='parser', default_timeout=60*60*24)


class Parser:
    def __init__(self, url: str):
        self.id = str(uuid4())
        self.url = self._format_url(url)
        self.result_path = self._create_folder_for_result()

    def parse(self) -> str:
        """returns path to downloaded content"""
        logger.info(f'try to parse {self.url}')
        start = datetime.utcnow()
        try:
            for url in self.get_child_urls(only_current=True):
                self.save_page(url)
        except Exception as e:
            logger.exception(e)
        zip_path = self._zip_content()
        end = datetime.utcnow()
        logger.info(f'time elapsed: {(end - start).total_seconds()} s.')
        return zip_path

    def get_child_urls(self, depth: int = 2, only_current=False) -> list:
        indexed = list()
        urls = [self.url]
        for i in range(depth):
            for url in urls:
                if url not in indexed:
                    indexed.append(self._format_url(url))
                    for child_url in self.parse_urls(url, tag='a'):
                        if not only_current or child_url.startswith(self.url):
                            indexed.append(child_url)

            urls = list(set(indexed))
        return indexed

    @staticmethod
    def _format_url(url: str) -> str:
        return url if '://' in url else 'https://' + url

    @staticmethod
    def get_site_name(url: str) -> str:
        return url if '://' not in url else url.split('://')[-1]

    def parse_urls(self, url: str, tag: str = 'a') -> list:
        logger.info(f'parse urls on {url} for tag {tag}')
        url = self._format_url(url)
        urls = list()
        try:
            soup = BeautifulSoup(
                requests.get(url, timeout=30).content, features="lxml")
        except requests.exceptions.RequestException as e:
            logger.error(f'cant open page {url} cause {e}')
        else:
            for data_url in soup(tag):
                for attr in ('src', 'href'):
                    if attr in dict(data_url.attrs):
                        full_url = urljoin(url, data_url[attr])
                        urls.append(full_url)
        logger.info(f'parsed urls: {urls}')
        return urls

    def download_data(self, url: str, path: str, ext: str = None):
        logger.info(f'downloading: {url}')
        filename = basename(url)
        if len(filename) > 0:
            filename = self._short(filename)
            if ext is not None:
                filename += f'.{ext}'
            try:
                content = requests.get(url, timeout=30).content
            except requests.exceptions.RequestException as e:
                logger.error(f'cant get data from url {url} cause {e}')
            else:
                with open(path + filename, 'wb') as data:
                    data.write(content)

    def save_page(self, url: str):
        logger.info(f'start saving page: {url}')
        page_name = self.get_site_name(url)
        page_name = self._short(page_name)
        path = self._create_folder_for_page(page_name)

        with concurrent.futures.ThreadPoolExecutor(max_workers=100) as executor:
            future = executor.submit(self.download_data, url, f'{self.result_path}/{page_name}/', ext='html')
            futures = [future]
            for tag in ('link', 'script', 'img'):
                for data_url in self.parse_urls(url, tag):
                    futures.append(
                        executor.submit(self.download_data, data_url, path=path))

    def _create_folder_for_page(self, page_name: str):
        path = f'{self.result_path}/{page_name}/files/'
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    @staticmethod
    def _short(name: str) -> str:
        return name if len(name) <= 200 else name[:200]

    def _zip_content(self) -> str:
        path = f'{self.result_path}.zip'
        logger.info(f'pack {path}')
        try:
            shutil.make_archive(
                self.result_path, 'zip',
                base_dir=self.id,
                root_dir='results')
            shutil.rmtree(self.result_path)
        except Exception as e:
            logger.exception(e)
        return path

    def _create_folder_for_result(self) -> str:
        path = f'./results/{self.id}'
        logger.info(f'create {path}')
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def _create_parse_job(self) -> Job:
        logger.info(f'create job {self.id}')
        job = Job.create(self.parse, connection=redis, id=self.id)
        queue.enqueue_job(job)
        return job

    @classmethod
    def get_results(cls, job_id: str) -> (str, bool):
        job = queue.fetch_job(job_id)
        if job is None:
            if f'{job_id}.zip' in os.listdir('results'):
                return job_id, True
            else:
                return 'error', False
        elif job.is_finished:
            return job.id, True
        else:
            return job.get_status(), False

    @classmethod
    def create_job(cls, url: str):
        parser = cls(url)
        parser._create_parse_job()
        return parser
