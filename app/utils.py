import logging
from logging.handlers import TimedRotatingFileHandler
from zipfile import ZipFile, ZIP_DEFLATED
import os


def rotator(source, dest):
    with ZipFile(f'{dest}.zip', mode='w', compression=ZIP_DEFLATED) as zf:
        zf.write(source, arcname=os.path.basename(source))
    os.remove(source)


def get_logger(name):
    if not os.path.exists('logs'):
        os.makedirs('logs')
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s')
    rh = TimedRotatingFileHandler(
        f'logs/{name}.log', when='d', interval=1, backupCount=30)
    rh.setFormatter(formatter)
    rh.rotator = rotator
    logger = logging.getLogger(name)
    if bool(os.getenv("DEBUG")):
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    logger.addHandler(rh)
    return logger


flask_logger = get_logger('flask')
parse_logger = get_logger('parser')